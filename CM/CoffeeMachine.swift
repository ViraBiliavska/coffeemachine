//
//  CoffeeMachine.swift
//  CM
//
//  Created by Mac on 29.07.2018.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit

class CoffeeMachine: NSObject {
    var waterAmount: Int
    var milkAmount: Int
    var coffeeAmount: Int
    var currentText: String
    init(waterAmount: Int, milkAmount: Int, coffeeAmount: Int, currentText: String) {
        self.waterAmount = waterAmount
        self.milkAmount = milkAmount
        self.coffeeAmount = coffeeAmount
        self.currentText = currentText
    }
    
    func displayTextAndResourses() {
        print(currentText)
        print("Water amount = \(waterAmount) \nMilk amount = \(milkAmount) \nCoffee amount = \(coffeeAmount) \n")
    }
    
    func addWater() {
        waterAmount += 300
        currentText = ""
        displayTextAndResourses()
    }
    
    func addMilk() {
        milkAmount += 200
        currentText = ""
        displayTextAndResourses()
    }
    
    func addCoffee() {
        coffeeAmount += 50
        currentText = ""
        displayTextAndResourses()
    }
    
    func espressoPortion() {
        waterAmount = waterAmount - 30
        coffeeAmount = coffeeAmount - 7
    }
    
    func espresso() {
        espressoPortion()
        currentText = "Take your espresso, please\n"
        displayTextAndResourses()
    }
    
    func americano() {
        espressoPortion()
        waterAmount = waterAmount - 100
        currentText = "Take your americano, please\n"
        displayTextAndResourses()
    }

    func latte() {
        espressoPortion()
        milkAmount = milkAmount - 80
        currentText = "Take your latte, please\n"
        displayTextAndResourses()
    }
    
    func checkWaterForEspressoOrLatte() {
        if waterAmount < 30 {
            currentText = "Water is not enough! Add water, please\n"
            print(currentText)
        }
    }
    
    func checkWaterForAmericano() {
        if waterAmount < 130 {
            currentText = "Water is not enough! Add water, please"
            print(currentText)
        }
    }
    
    func checkCoffee() {
        if coffeeAmount < 7 {
            currentText = "Coffee is not enough! Add coffee, please"
            print(currentText)
        }
    }
    
    func checkMilk() {
        if milkAmount < 80 {
            currentText = "Milk is not enough! Add milk, please"
            print(currentText)
        }
    }
}
        
        

    
    
    
    



    
    

