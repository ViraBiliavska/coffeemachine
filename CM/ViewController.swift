//
//  ViewController.swift
//  CM
//
//  Created by Mac on 29.07.2018.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var textToDisplay: UILabel!
    
    var coffeeMachine = CoffeeMachine.init(waterAmount: 150, milkAmount: 100, coffeeAmount: 20, currentText: "")
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func displayText() {
    textToDisplay.text = coffeeMachine.currentText
    }
    
    func checkCoffeeNeed() {
        coffeeMachine.checkCoffee()
        displayText()
    }
    
    func checkWaterForEspressoOrLatteNeed() {
        coffeeMachine.checkWaterForEspressoOrLatte()
        displayText()
    }
    
    func checkWaterForAmericanoNeed() {
        coffeeMachine.checkWaterForAmericano()
        displayText()
    }
    
    func checkMilkNeen() {
        coffeeMachine.checkMilk()
        displayText()
    }
    
    @IBAction func espressoDrink() {
        if coffeeMachine.waterAmount >= 30 && coffeeMachine.coffeeAmount >= 7 {
            coffeeMachine.espresso()
            displayText()
        } else {
            checkCoffeeNeed()
            checkWaterForEspressoOrLatteNeed()
        }
    }
    
    @IBAction func americanoDrink() {
        if coffeeMachine.waterAmount >= 130 && coffeeMachine.coffeeAmount >= 7 {
            coffeeMachine.americano()
            displayText()
        } else {
            checkCoffeeNeed()
            checkWaterForAmericanoNeed()
        }
    }
    
    @IBAction func latteDrink() {
        if coffeeMachine.waterAmount >= 30 && coffeeMachine.coffeeAmount >= 7 && coffeeMachine.milkAmount >= 80 {
            coffeeMachine.latte()
            displayText()
        } else {
            checkMilkNeen()
            checkCoffeeNeed()
            checkWaterForEspressoOrLatteNeed()
        }
    }
    
    @IBAction func water() {
        coffeeMachine.addWater()
        displayText()
    }
    
    @IBAction func milk() {
        coffeeMachine.addMilk()
        displayText()
    }
    
    @IBAction func coffee() {
        coffeeMachine.addCoffee()
        displayText()
    }
}

